
# vios_backup

[![Build Status](https://gitlab.com/arden-puppet/arden-vios_backup/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-vios_backup/commits/master) [![Puppet Forge](https://img.shields.io/puppetforge/v/arden/vios_backup.svg)](https://forge.puppetlabs.com/arden/vios_backup)

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with vios_backup](#setup)
    * [What vios_backup affects](#what-vios_backup-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with vios_backup](#beginning-with-vios_backup)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

This module configures a machine to remotely backup, retrieve, and then retain virtual and logical configuration backups from IBM Virtual I/O Servers (VIOS) in a PowerVM environment. 

These are config-only backups performed using the [viosbr](https://www.ibm.com/support/knowledgecenter/en/TI0002C/p8hcg/p8hcg_viosbr.htm) command. Note that there may be cases which necessitate `backupios` - this module does not support those forms of backups.

## Setup

### What vios_backup affects

This module changes the following components
* **Script Creation** - A backup script will be created in `/usr/local/bin` which will be used to execute the backup
* **User Creation** - The specified user is created with the provided SSH private and public keys
* **Cron Job** - One job will be scheduled to execute the overall backup process
* **Backup Directory** - The target directory will be created 

During the backup, an entry will be posted to the `user` syslog facility with either an `info` level for a success message or a `err` message for any kind of failure. Each entry will be tagged `vios_backup`.

### Setup Requirements

#### Backup User & Group

It is assumed that the specified backup user and group are created on the target machine prior to the application of this module. 

#### SSH Public Key Deployment

The public key provided during the configuration of this module must be added to the authorized keys file for each VIOS backed up by this instance.

1. Transfer a copy of your public key to the target server

    ```bash
    VIOS="power1-vio0.example.org"
    PUBLIC_KEY="${HOME}/.ssh/id_rsa.pub"
    scp "${PUBLIC_KEY}" "padmin@${VIOS}:/tmp/"
    ```
2. Log on to the VIOS and exit the restricted shell.

    ```bash
    ssh "padmin@${VIOS}"
    oem_setup_env
    ```
3. Append the contents of your key to padmin's authorized_keys file. 

    ```bash
    KEY_FILE="/tmp/id_rsa.pub"
    cat "${KEY_FILE}" >> /home/padmin/.ssh/authorized_keys
    ```

#### Required Applications

Nothing crazy:

* `/bin/logger` - needed to append messages to the syslog
* `/bin/sh` - the backup script is written targeting the borne shell
* `/bin/ssh` - we need ssh to communicate with the VIOS

### Beginning with vios_backup

Once the setup requirements are met, simply declare the class with all required parameters and you're good to go.

## Usage

At the moment, all parameters are mandatory. Here's a standard example:

```puppet
class { 'vios_backup':
  user           => 'padmin',
  group          => 'backup',
  base_directory => '/srv/backup/vios',
  days_retention => 90,
  target_vios    => [ 'host1-vio0.example.com', 'host1-vio1.example.com' ],
  cron_job       => {
    hour         => '1',
    minute       => '45',
    weekday      => '0',
  }
}
```

In the example above, two vio servers are backed up each week on Sunday at 1:45. After about two weeks of execution the directory may look similar to the following:

```bash
/srv/backup/vios
|-- host1-vio0.example.com
|   |-- 2018-12-02_0145.06.tar.gz
|   `-- 2018-12-09_0145.02.tar.gz
`-- host1-vio1.example.com
    |-- 2018-12-02_0145.26.tar.gz
    `-- 2018-12-09_0145.36.tar.gz
```

## Reference


## Limitations

* This has only been tested with VIOS 2.2.6.x - Assuming the viosbr command exists and works, this should work on all subsequent VIOS versions.

## Development

Submit a pull request.

## Contributors

Check out the [contributor list](https://gitlab.com/arden-puppet/arden-vios_backup/graphs/master)
