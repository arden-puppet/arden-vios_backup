#!/bin/sh

# Constants ---------------------------
TRUE=0
FALSE=1
BACKUP_PATTERN='Backup of this node ([[:alnum:].-]\{1,\}) successful'
VIOSBR_DIR='/home/padmin/cfgbackups'
SSH_CMD="ssh -o PasswordAuthentication=no"

# Capture Arguments
BACKUP_DIR=$1
RETAIN_DAYS=$2

# Backup VIOS =================================================================
# 
# Simple function to backup the specified VIO server using viosbr
backup_vios() {
        VIOS="${1}"
        ABORT="${FALSE}"
        TIMESTAMP=$(date +%Y-%m-%d_%H%M.%S)

        # Ensure the VIOS can be reached
        ping -c 3 "${VIOS}" 1>/dev/null 2>&1
        RC=$?
        if [ "${RC}" -ne "${TRUE}" ]; then
                ABORT="${TRUE}"
                MESSAGE="host could not be reached!"
        fi

        # Trigger the backup
        BACKUP_CMD="ioscli viosbr -backup -file ${TIMESTAMP}"
        if [ "${ABORT}" -ne "${TRUE}" ]; then
                # shellcheck disable=SC2029
                RESULT=$(${SSH_CMD} "padmin@${VIOS}" "${BACKUP_CMD}" 2>&1)
                RC=$?
                if [ "${RC}" -ne "${TRUE}" ]; then
                        ABORT="${TRUE}"
                        MESSAGE="viosbr ssh error: '${RESULT}'"
                fi

                # Ensure that we succeeded
                echo "${RESULT}" | \
                        grep -G "${BACKUP_PATTERN}" \
                        1>/dev/null 2>&1
                RC=$?
                if [ "${RC}" -ne "${TRUE}" ] && \
                        [ "${ABORT}" -ne "${TRUE}" ]; then
                        ABORT="${TRUE}"
                        MESSAGE="viosbr not succesful: '${RESULT}'"
                fi
        fi

        # Retrieve the backup
        BACKUP_FILE="${VIOSBR_DIR}/${TIMESTAMP}.tar.gz"
        if [ "${ABORT}" -ne "${TRUE}" ]; then
                RESULT=$(scp "padmin@${VIOS}:${BACKUP_FILE}" \
                        "${BACKUP_DIR}/${VIOS}" 2>&1)
                RC=$?
                if [ "${RC}" -ne "${TRUE}" ]; then
                        ABORT="${TRUE}"
                        MESSAGE="failed to transfer: '${RESULT}'"
                else 
                        MESSAGE="succeeded!"
                fi
        fi

        # Cleanup
        CMD="find ${VIOSBR_DIR} -type f -mtime +${RETAIN_DAYS} -exec rm {} \;"
        if [ "${ABORT}" -ne "${TRUE}" ]; then
                # shellcheck disable=SC2029
                ${SSH_CMD} "padmin@${VIOS}" "${CMD}"
        fi

        # Log the result
        if [ "${ABORT}" -eq "${TRUE}" ]; then
                PRIORITY="user.err"
        else
                PRIORITY="user.info"
        fi
        logger -t "vios_backup" -p "${PRIORITY}" "${VIOS}: ${MESSAGE}"
}

# Construct the list of VIOS machines 
VIOS_LIST=$(ls "${BACKUP_DIR}")
for VIOS in $VIOS_LIST
do
        backup_vios "${VIOS}"
done

# Cleanup old backups
find "${BACKUP_DIR}" -type f -mtime "+${RETAIN_DAYS}" \
        -exec rm {} \;
