require 'spec_helper'

describe 'vios_backup' do
  context 'valid configuration' do
    let(:params) do
      {
        user: 'backup',
        group: 'backup',
        base_directory: '/srv/backup/vios',
        days_retention: 14,
        target_vios: [
          'host1-vio0',
          'host1-vio1',
        ],
        cron_job: {
          hour: 1,
          minute: 0,
        },
      }
    end

    it { is_expected.to compile.with_all_deps }

    # Ensure the subclasses are present
    it {
      is_expected.to contain_class('vios_backup::install')
      is_expected.to contain_class('vios_backup::config')
    }

    # Backup script should exist
    it {
      is_expected.to contain_file('/usr/local/bin/vios_backup').with(
        ensure: 'file',
        owner: 'root',
        group: 'root',
        mode: '0755',
        checksum: 'sha256',
        checksum_value: 'acb562861089e88d4d6a7c783f24df848ab8ddbf39f480bd92b85a172ee31f71',
      )
    }

    # Cron job should be there and ready to go
    it {
      is_expected.to contain_cron('vios_backup_schedule').with(
        ensure: 'present',
        command: '/usr/local/bin/vios_backup /srv/backup/vios 14',
        user: 'backup',
        hour: 1,
        minute: 0,
      )
    }

    # Directory structure
    it {
      is_expected.to contain_file('/srv/backup/vios').with(
        ensure: 'directory',
        owner: 'backup',
        group: 'backup',
        mode: '0755',
      )
      is_expected.to contain_file('/srv/backup/vios/host1-vio1').with(
        ensure: 'directory',
        owner: 'backup',
        group: 'backup',
        mode: '0755',
      )
      is_expected.to contain_file('/srv/backup/vios/host1-vio1').with(
        ensure: 'directory',
        owner: 'backup',
        group: 'backup',
        mode: '0755',
      )
    }
  end
end
