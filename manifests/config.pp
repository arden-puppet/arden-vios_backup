# Schedules the backup job as specified in the main class
#
# @summary Creates a cron entry scheduling the VIOS backups.
class vios_backup::config {
  # Declare local variables so the command line isn't ridiculously long
  $retention = $vios_backup::days_retention
  $cmd = $vios_backup::backup_command
  $backup_dir = $vios_backup::base_directory

  # 
  cron { 'vios_backup_schedule':
    ensure  => present,
    command => "${cmd} ${backup_dir} ${retention}",
    user    => $vios_backup::user,
    *       => $vios_backup::cron_job,
  }
}
