# Performs config backups of a list of VIOS servers via passwordless SSH. This
# is accomplished using the viosbr command on each machine.
#
# @summary Configures remote backups of VIOS servers to the specified local disk repository.
#
# @param user [String]
#   Name of the local user who will own the backup files and run the scheduled
#   cron job.
#
# @param group [String]
#   Name of the local group which will be set on the backup directories. 
#
# @param base_directory [Stdlib::Absolutepath]
#   The root directory in which each vios backup will be stored. Note that this
#   must be an absolute path!
#
# @param days_retention [Integer]
#   Number of days to retain backups both on each VIOS and on the backup server.
#   Any backups older than this will be automatically deleted when the job
#   executes.
#
# @param target_vios [Array[Stdlib::Fqdn]]
#   Array of DNS names of VIOS which should be backed up. Currently all VIOS are
#   processed on the same schedule. Each entry specified here will generate a
#   subdirectory in `$base_directory` containing the backup files.
#
# @param cron_job [Hash[String, Variant[String, Integer, Array]]]
#   Specification of a cron resource that excludes the command and user. The
#   backup will be scheduled at the frequency specified in this structure.
#
# @example Daily backup of two VIOS with 14 days retention
#   class { 'vios_backup':
#     user           => 'backup',
#     group          => 'backup',
#     base_directory => '/srv/backup/vios',
#     days_retention => 14,
#     target_vios    => [ 'vio0.example.com', 'vio1.example.com' ],
#     cron_job       => {
#       hour         => '0',
#       minute       => '30',
#     },
#   }
class vios_backup (
  String $user                                            = undef,
  String $group                                           = undef,
  Stdlib::Absolutepath $base_directory                    = undef,
  Integer $days_retention                                 = undef,
  Array[Stdlib::Fqdn] $target_vios                        = undef,
  Hash[String, Variant[String, Integer, Array]] $cron_job = undef,
) inherits vios_backup::params {
  # Both internal classes must be contained
  contain vios_backup::install
  contain vios_backup::config

  # Ensure the ordering makes sense
  Class['vios_backup::install']
  -> Class['vios_backup::config']
}
