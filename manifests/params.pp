# Declares constants used within this module.
#
# @summary Declare constants used within this module.
#
# @param local_bin [Stdlib::Absolutepath]
#   Path to the local binary directory on this host. For most operating
#   systems the value is `/usr/local/bin`
class vios_backup::params (
  Stdlib::Absolutepath $local_bin = undef,
){
  $backup_command = "${local_bin}/vios_backup"
}
