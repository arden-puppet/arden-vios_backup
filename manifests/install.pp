# Performs preliminary setup needed for the operation of the backups on a given
# host. Note that the existence of the backup user and group is assumed.
#
# @summary Creates the backup directory structure and installs the script.
class vios_backup::install {
  # Declare the base backup directory
  file { $vios_backup::base_directory:
    ensure => directory,
    owner  => $vios_backup::user,
    group  => $vios_backup::group,
    mode   => '0755',
  }

  # Create a sub directory for each target VIO server
  $vios_backup::target_vios.each | $vios | {
    file { "${vios_backup::base_directory}/${vios}":
      ensure => directory,
      owner  => $vios_backup::user,
      group  => $vios_backup::group,
      mode   => '0755',
    }
  }

  # Install the backup script
  file { $vios_backup::backup_command:
    ensure         => file,
    owner          => 'root',
    group          => 'root',
    mode           => '0755',
    checksum       => 'sha256',
    checksum_value => 'acb562861089e88d4d6a7c783f24df848ab8ddbf39f480bd92b85a172ee31f71',
    content        => file('vios_backup/vios_backup.sh'),
  }
}
